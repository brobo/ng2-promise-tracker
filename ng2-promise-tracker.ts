import { Injectable } from 'angular2/angular2';

@Injectable()
export class PromiseTracker<T> {
       
    promise: Promise<T>;
    active: boolean;

    constructor(): void {
        promise = null;
        active = false;
    }

    listen(promise: Promise<T>): void {
        this.promise = promise; 
        active = true;
        this.promise.then(x => {
            active = false;
            return new Promise((resolve, reject) => resolve(x));
        }, x => {
            active = false;
            return new Promise((resolve, reject) => reject(x));
        });
    }
}

